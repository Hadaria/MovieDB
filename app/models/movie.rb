class Movie < ActiveRecord::Base

  def self.search(search)
    if search
      where(["title LIKE ?","%#{search}%"])
    else
      all
    end
  end
  attr_accessible :title, :body, :year, :actors, :directors, :categories
end
