class Search < ActiveRecord::Base

  def search_movies
    movies = Movie.all

    movies = movies.where(["title LIKE ?", "%#{keywords}%"]) if keywords.present?
    movies = movies.where(["year LIKE ?", "%#{year}%"]) if year.present?
    movies = movies.where(["actors LIKE ?", "%#{actors}%"]) if keywords.present?
    movies = movies.where(["directors LIKE ?", "%#{directors}%"]) if directors.present?
    movies = movies.where(["categories LIKE ?", categories]) if categories.present?
    return movies
  end

  attr_accessible :actors, :categories, :directors, :keywords, :year
end
