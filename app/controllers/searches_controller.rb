class SearchesController < ApplicationController
  
  def new
    @search = Search.new
    @categories = Categories.uniq.pluck(:title)
  end

  def create
    @search = Search.create(params[:search])
    redirect_to @search
  end

  def show
    @search = Search.find(params[:id])
  end

  private
  def search_params
    params.require(:search).permit(:keywords, :year, :actors, :directors, :categories)
  end
end
