class CreateMovies < ActiveRecord::Migration
  def change
    create_table :movies do |t|
      t.string          :title
      t.integer         :year
      t.text            :actors
      t.text            :directors
      t.text            :categories

      t.timestamps
    end
  end
end
