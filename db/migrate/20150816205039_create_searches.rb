class CreateSearches < ActiveRecord::Migration
  def change
    create_table :searches do |t|
      t.string :keywords
      t.integer :year
      t.text :actors
      t.text :directors
      t.text :categories

      t.timestamps
    end
  end
end
