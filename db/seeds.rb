# -*- coding: utf-8 -*-
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

movie_1 = Movie.create(title: "Top Gun",
                       year: 1986, 
                       actors: "Tom Cruise, Kelly McGillis, Val Kilmer, Anthony Edwards, Michael Ironside, James Tolkan", 
                       directors: "Tony Scott",
                       categories: "Action, Drame, Romance")

movie_2 = Movie.create(title: "Iron Man", 
                       year: 2008,
                       actors: "Robert Downey Jr., Gwyneth Paltrow, Jon Favreau, Terrence Howard, Jeff Bridge",
                       directors: "Jon Favreau",
                       categories: "Action, Science-Fiction")


movie_3 = Movie.create(title: "eXistenZ", 
                       year: 1999,
                       actors: "Jude Law, Jennifer Jason Leigh, Willem Dafoe, Don McKellar, Ian Holm",
                       directors: "David Cronenberg",
                       categories: "Science-Fiction")


movie_4 = Movie.create(title: "Le Seigneur des Anneaux : Les Deux Tours", 
                       year: 2002,
                       actors: "Ian Holm, Elijah Wood, Ian McKellen, Viggo Mortensen, Christopher Lee, Orlando Bloom",
                       directors: "Peter Jackson",
                       categories: "Action, Aventure, Fantastique")

cat_1 = Categories.create(title: "Action")
cat_2 = Categories.create(title: "Animation")
cat_3 = Categories.create(title: "Arts Martiaux")
cat_4 = Categories.create(title: "Aventure")
cat_5 = Categories.create(title: "Biopic")
cat_6 = Categories.create(title: "Bollywood")
cat_7 = Categories.create(title: "Classique")
cat_8 = Categories.create(title: "Comédie")
cat_9 = Categories.create(title: "Comédie Dramatique")
cat_10 = Categories.create(title: "Comédie Musicale")
cat_11 = Categories.create(title: "Concert")
cat_12 = Categories.create(title: "Dessin Animé")
cat_13 = Categories.create(title: "Divers")
cat_14 = Categories.create(title: "Documentaire")
cat_15 = Categories.create(title: "Drame")
cat_16 = Categories.create(title: "Epouvannte / Horreur")
cat_17 = Categories.create(title: "Espionnage")
cat_18 = Categories.create(title: "Expérimental")
cat_19 = Categories.create(title: "Famille")
cat_20 = Categories.create(title: "Fantastique")
cat_21 = Categories.create(title: "Guerre")
cat_22 = Categories.create(title: "Historique")
cat_23 = Categories.create(title: "Judiciaire")
cat_24 = Categories.create(title: "Musical")
cat_25 = Categories.create(title: "Opéra")
cat_26 = Categories.create(title: "Péplum")
cat_27 = Categories.create(title: "Policier")
cat_28 = Categories.create(title: "Romance")
cat_29 = Categories.create(title: "Science-Fiction")
cat_30 = Categories.create(title: "Show")
cat_31 = Categories.create(title: "Sport")
cat_32 = Categories.create(title: "Thriller")
cat_33 = Categories.create(title: "Western")
